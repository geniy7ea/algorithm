#include <iostream>
#include <algorithm>
#include <vector>

struct Info
{
    //using str = std::string;

    //str     name;
    int     age;
    //time_t  regTime;
};

void fetchInfo(std::vector<Info>& data)
{
    data.push_back({1});
    data.push_back({2});
    data.push_back({3});
    data.push_back({4});
    data.push_back({5});
    data.push_back({6});
    data.push_back({7});
    data.push_back({8});
    data.push_back({9});
    data.push_back({10});
}
//
//int main()
//{
//    std::vector<Info> data;
//    fetchInfo(data);
//
//    for (auto& val : data)
//    {
//        std::cout << val.age << " ";
//    }
//
//    data.erase(std::remove_if(data.begin(), data.end(), [](const Info& value)
//        {
//            return value.age > 3;
//        }
//    ), data.end());
//
//    std::cout << std::endl;
//    for (auto& val : data)
//    {
//        std::cout << val.age << " ";
//    }
//
//    return 0;
//}

//struct Info
//{
//    using str = std::string;
//
//    str     name;
//    int     age;
//    time_t  regTime;
//};
//
//void fetchInfo(std::vector<Info>& data)
//{
//    data.push_back({ "Nikita",   33, 100 });
//    data.push_back({ "Anna",     18, 200 });
//    data.push_back({ "Egor",     26, 300 });
//    data.push_back({ "Alexandr", 20, 400 });
//}