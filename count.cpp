#include <iostream>
#include <unordered_map>
#include <algorithm>

//int main()
//{
//    std::unordered_map<char, int> m;
//
//    std::string s("abcba");
//    std::for_each(s.begin(), s.end(), [&m](char& c) { m[c]++; });
//
//    char ch = 's';
//
//    int count = std::count_if(m.begin(), m.end(),
//        [&ch](std::pair<const char, int>& entry) {
//            return (entry.first == ch);
//        });
//
//    if (count) {
//        std::cout << "Key found";
//    }
//    else {
//        std::cout << "Key not found";
//    }
//
//    return 0;
//}