#include <iostream>
#include <thread>
#include <bitset>

using namespace std;

template <class T, size_t N>
class container
{
    alignas(alignof(T)) uint8_t arr[N * sizeof(T)] = { 0 };
    bitset<N> init;

public:
    T& operator[] (size_t n)
    {
        if (n >= init.size())
            throw out_of_range{ "Out of range " };

        T* it = reinterpret_cast<T*>(arr) + n;

        // ����� ������������ ��� ������ ��������� � ��������
        if (!init[n])
        {
            uninitialized_default_construct(it, it + 1);
            init[n] = true;
        }

        return *it;
    }

    ~container()
    {
        // ����� ������������ ��� ������� ���������� �������� ����������
        for (size_t i = 0; i < init.size(); ++i)
        {
            if (init[i])
            {
                T* it = reinterpret_cast<T*>(arr) + i;
                destroy_at(it);
            }
        }
    }
};

class CData
{
    int m_nData = 0;

public:
    CData()
    {
        using namespace chrono_literals;

        // �������� ������� �������� 2 �������

        cout << "Construction started";

        for (int i = 0; i < 4; ++i)
        {
            this_thread::sleep_for(500ms);
            cout << (i < 3 ? "." : " Done\n");
        }
    };

    ~CData()
    {
        std::cout << "Destroyed " << m_nData << endl;
    };

    operator int() const
    {
        return m_nData;
    }

    CData& operator= (int n)
    {
        m_nData = n;

        return *this;
    }
};

int main()
{
    container<CData, 5> c;

    cout << "a--> " << c[1] << " <--a\n\n";

    c[1] = 5;
    cout << "b--> " << c[1] << " <--b\n\n";

    cout << "c--> ";
    c[3] = 7;
    cout << "<--c\n\n";

    cout << "d--> " << c[3] << " <--d\n\n";

    return 0;
}