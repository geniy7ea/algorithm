#include <iostream>
#include <vector>

using namespace std;

class CData
{
    int* m_pData = nullptr;

public:
    CData(int n = 0)
    {
        //cout << "CData(int n = 0) " << n << "\n";
        m_pData = new int;
        *m_pData = n;
    }

    CData(const CData& r)
    {
        //cout << "CData(const CData & r) " << *r.m_pData << "\n";
        m_pData = new int;
        *m_pData = *r.m_pData;
    }

    CData& operator=(const CData& r)
    {
        //cout << "operator= " << *r.m_pData << "\n";
        *m_pData = *r.m_pData;

        return *this;
    }

    int get() const
    {
        return *m_pData;
    }

    ~CData()
    {
        //cout << "~CData " << *m_pData << "\n";
        delete m_pData;
    }
};


void print(const char* pszTxt, const vector<CData>& data)
{
    cout << pszTxt << ":";

    for (auto& d : data)
        cout << " " << d.get();

    cout << "\n";
}

//int main()
//{
//    vector<CData> v1 = { 1, 2, 3 };
//    vector<CData> v2;
//
//    v2.resize(v1.size());
//
//    copy(v1.cbegin(), v1.cend(), v2.begin());
//
//    print("v1", v1);
//
//    v1 = { 4, 5, 6 };
//
//    print("v1", v1);
//    print("v2", v2);
//
//    return 0;
//}