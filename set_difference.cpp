﻿#include <iostream>     
#include <algorithm>    // std::set_difference, std::sort
#include <vector>       

bool myfunction(int i, int j) { return i < j; }

//int main() {
//    int first[] = { 5,10,15,20,25,30,40,50,14 };
//    int second[] = { 50,40,30,20,10 };
//    std::vector<int> v(14);                      // 0  0  0 ...
//    std::vector<int>::iterator it;
//
//    std::sort(first, first + 9);     //  5 10 15 20 25
//    std::sort(second, second + 5);   // 10 20 30 40 50
//
//    // using default comparison:
//    if(std::includes(first, first + 9, second, second + 5))
//        std::cout << "first includes second!\n";
//
//    // using myfunction as comp:
//    if(std::includes(first, first + 9, second, second + 5, myfunction));
//    std::cout << "first includes second!\n";
//
//    it = std::set_difference(first, first + 9, second, second + 5, v.begin());
//    //  5 14 15 25  0  0  ...
//    v.resize(it - v.begin());                      //  5 14 15 25
//
//    std::cout << "The difference has " << (v.size()) << " elements:\n";
//    for (it = v.begin(); it != v.end(); ++it)
//        std::cout << ' ' << *it;
//    std::cout << '\n';
//
//    return 0;
//}
