﻿#include <algorithm>
#include <iostream>
#include <set>

using namespace std;

void print(int x)
{
    cout << x << " ";
}

// Function to display the
// elements of set
void display(set<int> s)
{
    for_each(s.begin(), s.end(),
        print);
}

//int main()
//{
//    // Empty set container
//    set<int> s;
//
//    // Insert elements in random order
//    s.insert(10);
//    s.insert(20);
//    s.insert(30);
//    s.insert(40);
//    s.insert(50);
//
//    // Invoking display() function
//    display(s);
//    return 0;
//}
