#include <iostream>
#include <algorithm>
#include <functional>
#include <vector>

template<class T>
void transformWith(std::vector<T>& arr, std::function<T(const T& ref)> func)
{
    std::transform(arr.begin(), arr.end(), arr.begin(), func);
}

template<class T>
void transformWith1(std::vector<T>& arr, std::function<T(const T& lhs, const T& rhs)> func)
{
    std::transform(arr.cbegin() +1, arr.cend(), arr.cbegin(), arr.begin(), func);
}

//int main()
//{
//    std::vector<int> ints{ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
//    transformWith<int>(ints, [](const int& ref)
//        {
//            return ref * 2;
//        });
//
//    std::vector<int> sum{ 1, 2, 3, 4, 5 };
//    transformWith1<int>(sum, [](const int& lhs, const int& rhs)
//        {
//            return lhs + rhs;
//        });
//
//    for (auto i : sum)
//    {
//        std::cout << i << " ";
//    }
//    return 0;
//}

 